package com.duanjy.webtools.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.duanjy.webtools.model.Cmdinfo;
import com.duanjy.webtools.model.Hostinfo;
import com.jfinal.plugin.activerecord.Page;

public class CommandInfoService {
	private static final Logger logger = Logger.getLogger(HostInfoService.class);
			
	public Page<Cmdinfo> getCmdService(int page, int limit,String searKey ) {
		Page<Cmdinfo> cmdInfo = new Page<Cmdinfo>();

		// searKey ==null 的顺序必须出现在前面，不然会抛出java.lang.NullPointerException
		if (searKey == null || searKey.length() <= 0) {
			String select = "select * ";
			String sqlExceptSelect = "from cmdinfo order by id desc";
			cmdInfo = Cmdinfo.dao.paginate(page, limit, select, sqlExceptSelect);
					
		} else {
			String select = "select * ";
			String sqlExceptSelect = "from cmdinfo where id like ? or cmd_content like ? or cmd_name like ? "
					+ "or cmd_mark like ? order by id desc";
			cmdInfo = Cmdinfo.dao.paginate(page, limit, select,
					sqlExceptSelect, "%" + searKey + "%", "%" + searKey + "%",
					"%" + searKey + "%", "%" + searKey + "%");
		}
		return cmdInfo;
	}
	//获取命令列表
	public List<Cmdinfo> getCmdList() {
		List<Cmdinfo> cmdInfo = new ArrayList<Cmdinfo>();
		
		String sql = "select cmd_name,cmd_content from cmdinfo";
		cmdInfo = Cmdinfo.dao.find(sql);
		
		return cmdInfo;
	}
	//添加命令
	public Boolean addCmdService(Cmdinfo cmdinfo){
		Boolean status = cmdinfo.save();
		return status;
	}
}
