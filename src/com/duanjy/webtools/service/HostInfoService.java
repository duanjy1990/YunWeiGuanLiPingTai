package com.duanjy.webtools.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.duanjy.webtools.controller.HostManageController;
import com.duanjy.webtools.model.Hostinfo;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

public class HostInfoService {
	private static final Logger logger = Logger.getLogger(HostInfoService.class);
	//获取主机信息
	public Page<Hostinfo> getHostService(int page,int limit,String searKey){
		Page<Hostinfo> hostinfo = new Page<Hostinfo>();
		
		//searKey ==null 的顺序必须出现在前面，不然会抛出java.lang.NullPointerException
		if( searKey == null || searKey.length() <= 0){
			String select = "select * ";
			String sqlExceptSelect = "from hostinfo order by id desc"; 
			hostinfo = Hostinfo.dao.paginate(page, limit, select, sqlExceptSelect);	
		}else{
			String select = "select * ";
			String sqlExceptSelect = "from hostinfo where id like ? or host_group like ? or ip_address like ? "
					+ "or mark like ? order by id desc"; 
			hostinfo = Hostinfo.dao.paginate(page, limit, select, sqlExceptSelect,"%"+searKey+"%","%"+searKey+"%",
					"%"+searKey+"%","%"+searKey+"%");	
		}
		
		return hostinfo;
		
	}
	//根据主机id获取主机信息
	public Hostinfo getColumns(int hostid){
		Hostinfo hostinfo = new Hostinfo();
		hostinfo = Hostinfo.dao.findByIdLoadColumns(hostid, "ip_address,login_name,login_pwd,login_port");
		return hostinfo;
	}
	
	//插入主机信息
	public Boolean addHostService(Hostinfo hostinfo){
		Boolean status = hostinfo.save();
		return status;
	}
	
	//删除主机信息
	public Boolean delHostService(int hostId){
		Boolean status = Hostinfo.dao.deleteById(hostId);
		logger.info("delHostService-status:"+status);
		return status;
	}
	//根据id值更新主机连接状态
	public Boolean updateConnStaus(int hostid,int connstatus){
		
		//logger.info("updateConnStaus-connstatus:"+connstatus);
		Boolean status = Hostinfo.dao.findById(hostid).set("conn_status", connstatus).update();
		logger.info("updateConnStaus-status:"+status);
		return status;
	}
	
}
