package com.duanjy.webtools.interceptor;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class GlobalInterceptor implements Interceptor {
	private static final Logger logger = Logger.getLogger(GlobalInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		
		// TODO Auto-generated method stub
		try {
			inv.invoke();
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(inv.getActionKey() + " ִ�г���.", e);
			JSONObject object = new JSONObject();
			object.put("return_code", "1111");
			object.put("return_msg", e.getMessage() == null ? "δ֪����" : e.getMessage());
			
			inv.getController().renderJson(object);
		}
	}

}