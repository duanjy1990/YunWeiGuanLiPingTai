package com.duanjy.webtools.config;


import com.duanjy.webtools.controller.FileManageController;
import com.duanjy.webtools.controller.HostManageController;
import com.duanjy.webtools.controller.SysManageController;
import com.duanjy.webtools.controller.ToolManageController;
import com.duanjy.webtools.interceptor.GlobalInterceptor;
import com.duanjy.webtools.model.Cmdinfo;
import com.duanjy.webtools.model.Hostinfo;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.Const;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;

public class BaseConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		// TODO Auto-generated method stub
		PropKit.use("mysql_db.properties","utf-8");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		
		//设置上传文件大小限制
		me.setMaxPostSize(1000*Const.DEFAULT_MAX_POST_SIZE);
		
		
	}

	@Override
	public void configRoute(Routes me) {
		// TODO Auto-generated method stub
		//主机配置类请求
		me.add("/host", HostManageController.class);
		//工具类请求
		me.add("/tool",ToolManageController.class);
		//文件类请求
		me.add("/file",FileManageController.class);
		//系统设置
		me.add("/setup",SysManageController.class);

	}

	@Override
	public void configEngine(Engine me) {
		// TODO Auto-generated method stub

	}

	@Override
	public void configPlugin(Plugins me) {
		String url = PropKit.get("jdbcUrl");
		String username = PropKit.get("username");
		String password = PropKit.get("password");
		DruidPlugin druidPlugin = new DruidPlugin(url, username, password);
		me.add(druidPlugin);
		
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		me.add(arp);
		
		//当新增表数据时需要添加
		arp.addMapping("hostinfo", Hostinfo.class);
		arp.addMapping("cmdinfo", Cmdinfo.class);
		
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO Auto-generated method stub
		me.addGlobalActionInterceptor(new GlobalInterceptor());

	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub

	}
	
	public static void main(String[] args) {
		JFinal.start("WebRoot", 80, "/", 5);
	}

	
	
}
