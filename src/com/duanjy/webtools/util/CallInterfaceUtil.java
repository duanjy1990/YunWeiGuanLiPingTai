package com.duanjy.webtools.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.omg.CORBA.PRIVATE_MEMBER;




public class CallInterfaceUtil {
	public static Logger logger = Logger.getLogger(CallInterfaceUtil.class);
	
	//POST方式调用远程接口
	public Map httpClientPost(String url,String contentype,String parameter){
	    long startTime = 0;
		long endTime = 0;
		int execTime =0;
		String result = "";
		Map resMap = new HashMap();
		
		
	
		try {
			//记录接口开始调用时间
			startTime = System.currentTimeMillis();
            URL targetUrl = new URL(url);
            HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
            httpConnection.setDoOutput(true);
            //请求方式为POST
            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-type", contentype);
//            httpConnection.setConnectTimeout(timeout);
//            httpConnection.setReadTimeout(2000);
            
            OutputStream outputStream = httpConnection.getOutputStream();
            outputStream.write(parameter.getBytes("UTF-8"));
            outputStream.flush();

            if (httpConnection.getResponseCode() != 200) {
            	//当返回码不是200时，打印错误码和具体错误信息
            	//result = "{\"HttpCode\":"+ httpConnection.getResponseCode()+"},";
            	
            	if( httpConnection.getResponseCode() ==  404 ){
            		result =  "{\"return_msg\": \"404 Not Found\"}";
            		
            	}else{
            		BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                            (httpConnection.getErrorStream())));
    		
    		        String output = "";
    		        while ((output = responseBuffer.readLine()) != null) {
    		          	result += output;
    		          }    
            	}
            	
            	resMap.put("httpcode",httpConnection.getResponseCode());
            	resMap.put("message", result);
            	logger.debug("接口返回util："+resMap.get("message"));
            	return resMap;
            }

            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                          (httpConnection.getInputStream())));

            String output;
            while ((output = responseBuffer.readLine()) != null) {
            	result += output;
            }
            //记录接口调用结束时间
            endTime=System.currentTimeMillis();
            httpConnection.disconnect();

       } catch (MalformedURLException e) {
            e.printStackTrace();
            result = "{\"return_msg\": \"请求url异常...\"}";
            resMap.put("message", result);
            logger.debug("MalformedURLException："+resMap.get("message"));
        	return resMap;
            
       } catch (IOException e) {
            e.printStackTrace();
            result  = "{\"return_msg\": \"connection refused,please check your network or address !!!\"}";
            resMap.put("message", result);
            logger.debug("IOException："+resMap.get("message"));
        	return resMap;
      }

		execTime = (int)(endTime-startTime);
		//返回map中添加请求返回码、请求返回结果、请求返回时间
		resMap.put("httpcode", "200");	
		resMap.put("message", result);
		resMap.put("time", execTime);
		
		logger.debug("接口返回util："+resMap.get("message"));
		logger.debug("接口调用耗时:"+resMap.get("time") +" ms");
		logger.info("接口返回:"+resMap);
		return resMap;
	}
	//GET方法调用远程接口
	public String httpClientGet(String url,String contentype){
		 String reString = "";
		 try {

             URL restServiceURL = new URL(url);

             HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
             httpConnection.setRequestMethod("GET");
             httpConnection.setRequestProperty("Accept", contentype);

             if (httpConnection.getResponseCode() != 200) {
            	 reString = "Failed : HTTP error code : "+ httpConnection.getResponseCode();
             	 return reString;
             }

             BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                    (httpConnection.getInputStream())));

             String output;
             while ((output = responseBuffer.readLine()) != null) {
                    reString += output +"\n";
             }

             httpConnection.disconnect();

        } catch (MalformedURLException e) {
             e.printStackTrace();
             reString = "请求url异常...";
             return reString;

        } catch (IOException e) {
             e.printStackTrace();
             reString = "connection refused,please check your network or url!!!";
             return reString;
        }
		 return reString;
	}
}
