package com.duanjy.webtools.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtil {
    public static final FileUtil instance = new FileUtil();

    // 随机一个文件名
    public String randomFileName() {
        Date dt = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyMMddHHmmssSSS");
        String fileName = sdf.format(dt);
        return fileName;
    }

    /**
     * 修改文件名
     * @param filePath
     *            eg:D:/gai.jpg
     * @return
     */
    public String changeFileName(String filePath) {
        File file = new File(filePath);// 指定文件名及路径
        String name = randomFileName() + ".xlsx";
        //文件夹位置
        String path = filePath.substring(0, filePath.lastIndexOf("\\"));
        String newFilePath = path+"\\"+name;
        file.renameTo(new File(newFilePath)); // 改名
        return name;
    }
}