package com.duanjy.webtools.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import com.duanjy.webtools.controller.HostManageController;
import com.duanjy.webtools.model.Hostinfo;
import com.duanjy.webtools.service.HostInfoService;
import com.jcraft.jsch.Session;

public class MultiInsertHosts {
	private static final Logger logger = Logger.getLogger(MultiInsertHosts.class);
	String[] hostList = null;
	String[] hostElement = null;
	
	public Integer[] BatchInsert(String string) throws InterruptedException{
		Integer totalCount = 0;
		Integer successCount = 0;
		Integer failCount = 0;
		
		final Integer[] count = new Integer[]{totalCount,successCount,failCount};
		//根据回车符将前台接收的主机列表分割成数组
	    hostList = string.split("\n");	
	    count[0] = hostList.length;
		//使用CountDownLatch 判断子线程是否执行完成
		final CountDownLatch countDownLatch = new CountDownLatch(count[0]);
		final Object object = new Object();
		final HostInfoService hostInfoService = new HostInfoService();
		
		for(final String hostString:hostList){
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					//将主机列表中的每个元素进行拆解，并组装成对象；
					hostElement = hostString.split(",");
					
					Hostinfo hostinfo = new Hostinfo().setIpAddress(hostElement[0]).setLoginPort(Integer.parseInt(hostElement[1]))
							.setLoginName(hostElement[2]).setLoginPwd(hostElement[3]).setHostType(hostElement[4])
							.setHostGroup(hostElement[5]).setMark(hostElement[6]).setCreateTime(new Date())
							.setModifyTime(new Date());
					
					int conn_status = 1;
					//判断主机连接状态
					SshdService service = new SshdService();
					Session session = service.sshd(hostElement[0], hostElement[2], hostElement[3],
							Integer.parseInt(hostElement[1]));
					if (session.isConnected()) {
						conn_status = 0;
					}
					hostinfo.setConnStatus(conn_status);
					
					//调用数据库service层开始插入数据库
					Boolean status = hostInfoService.addHostService(hostinfo);
					
					if( status ){						
							count[1] ++;			
					} else {
						count[2] ++;	
					}
					//线程工作完成，计数器值减一
					countDownLatch.countDown();
				}
			}).start();
			
			
		}
		countDownLatch.await();	
		return count;
	}
}
