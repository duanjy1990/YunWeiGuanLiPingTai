package com.duanjy.webtools.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

public class RemoteCmdExecUtil {
	public String resultMsg = "";
	public static Logger log = Logger.getLogger(RemoteCmdExecUtil.class);
	Connection conn = null;
	Session sess = null;
	BufferedReader bufferedReader = null;
	BufferedReader errBufferedReader = null;
	
	public String cmdExec(String hostname,String username,String password,int port,String cmd) throws IOException{
			
		try {
			//根据主机地址和端口创建一个新的实例
		    conn = new Connection(hostname, port);
			conn.connect();
			
			boolean isAuthenticated = conn.authenticateWithPassword(username, password);
	
			if (isAuthenticated == false){
				resultMsg = "用户名和密码校验失败...\n";
			}
		
			
			/* Create a session */
			sess = conn.openSession();
			sess.execCommand(cmd);

			//创建输入输出流
			InputStream stdout = new StreamGobbler(sess.getStdout());
			//创建错误输入输出流
			InputStream stderr = new StreamGobbler(sess.getStderr());
			
		    bufferedReader = new BufferedReader(new InputStreamReader(stdout,"utf-8"));
		    errBufferedReader = new BufferedReader(new InputStreamReader(stderr,"utf-8"));

			resultMsg += getOutString(errBufferedReader);
			resultMsg += getOutString(bufferedReader);
		
			//关闭连接
			sess.close();
			conn.close();
			
		} catch (IOException e) {
			//e.printStackTrace(System.err);
			resultMsg = "主机连接异常,请稍后再试\n";
		}
		
		log.debug("命令执行结果:"+resultMsg);
		return resultMsg;
	}
	
	public String getOutString(BufferedReader br) throws IOException{
		String resString = "";
		String line = null;
		while ( (line = br.readLine()) != null ) {
			resString += line + "\n";	
		}
		return resString;

	}
}
