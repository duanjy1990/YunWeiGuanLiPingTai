package com.duanjy.webtools.util;

import java.util.concurrent.CountDownLatch;

import com.duanjy.webtools.model.Hostinfo;
import com.duanjy.webtools.service.HostInfoService;


public class MultiDeleteHosts {
    int suCount = 0;
    int faCount = 0;
    int total = 0;
	public Integer[] batchDel(String hostids) throws InterruptedException{
		
		String[] idArray = hostids.split(",");
	    total = idArray.length;
	    
		final Integer[] result = new Integer[]{total,suCount,faCount};
		final HostInfoService hostInfoService = new HostInfoService();
		final CountDownLatch countDownLatch = new CountDownLatch(total);
		
		for(final String id : idArray){
			new Thread(new Runnable() {
				@Override
				public void run() {
					int new_id = Integer.parseInt(id.replaceAll("\"", ""));
	
					//调用dao层，删除主机
					Boolean status = hostInfoService.delHostService(new_id);
					if( status ){
						suCount ++;
					}else{
						faCount ++;
					}
					
					// 线程工作完成，计数器值减一
					countDownLatch.countDown();
					
				}
			}).start();
		
		}
		countDownLatch.await();	
		return result;
	}
}
