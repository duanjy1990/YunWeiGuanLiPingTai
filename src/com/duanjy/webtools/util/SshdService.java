package com.duanjy.webtools.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;

import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;

import sun.util.logging.resources.logging;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SshdService {
	/**
	 * 密码方式登录
	 * 
	 * @param hostip
	 * @param username
	 * @param passwd
	 * @param port
	 * @param sPath
	 * @param dPath
	 */
	public static Logger log = Logger.getLogger(SshdService.class);
	public Session session = null;
	public int timeOut = 5000;
	
	public Session sshd(String hostip, String username, String passwd, int port) {
		JSch jsch = new JSch();
		try {
			// 采用默认端口进行连接
			if (port <= 0) {
				session = jsch.getSession(username, hostip);
			} else {
				// 采用指定的端口连接服务器
				session = jsch.getSession(username, hostip, port);
			}
			// 如果服务器连接不上，则抛出异常
			// if (session == null) {
			// throw new
			// Exception("session is null,"+hostip+" connect failed!!");
			// }
			// 设置登陆主机的密码
			session.setPassword(passwd);// 设置密码
			// 设置第一次登陆的时候提示，可选值：(ask | yes | no)
			session.setConfig("StrictHostKeyChecking", "no");
			// 设置登陆超时时间
			try {
				session.connect(timeOut);
			} catch (Exception e) {
				throw new Exception("主机连接失败!!");
			}

			log.info(hostip + " connect success!!!");
		} catch (Exception e) {
			e.printStackTrace();
			return session;
		}

		return session;

	}

	
}
