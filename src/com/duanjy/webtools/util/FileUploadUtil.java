package com.duanjy.webtools.util;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;

import org.apache.log4j.Logger;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;

public class FileUploadUtil {
	/*
	 * status = 0 上传成功 status = 1 其他异常 status = 2 主机连接失败 status = 3 文件不存在
	 */
	static int status;
	public Connection conn = null;
	public boolean isAuthenticated;
	public SCPClient scpClient = null;
	public static Logger log = Logger.getLogger(FileUploadUtil.class);
	
	public int sftpFile(String hostname, String username, String password,
			int port, String spath, String dpath) {

		Connection conn = new Connection(hostname, port);
		try {
			conn.connect();
			isAuthenticated = conn.authenticateWithPassword(username, password);
		} catch (IOException e) {
			e.printStackTrace();
			return status = 3;
			
		}

		try {
			scpClient = conn.createSCPClient();
			// 从本地复制文件到远程目录
			scpClient.put(spath, dpath);
		} catch (IOException e) {
			e.printStackTrace();
			return status = 2;
		}
		
		status = 0;
		conn.close();

		log.info("文件上传status:"+status);
		return status;
	}
}
