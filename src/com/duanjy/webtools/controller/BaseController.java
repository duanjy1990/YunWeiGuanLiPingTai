package com.duanjy.webtools.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.druid.support.json.JSONParser;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.json.Json;

public class BaseController extends Controller{
	
	Map map = new HashMap();
	public Map getResult(String code,String msg,int count,List data){
		map.put("code", code);
		map.put("msg", msg);
		map.put("count", count);
		map.put("data", data);
		
		return map;
	}	
	
	public JSONObject getJsonData(String code,String msg,int count,Map data){
		JSONObject resJson = new JSONObject();
		resJson.put("code", code);
		resJson.put("msg", msg);
		resJson.put("count", count);
		resJson.put("data", data);
		
		return resJson;
	}
	public JSONObject getJsonResult(String code,String msg){
		JSONObject resJson = new JSONObject();
		resJson.put("return_code", code);
		resJson.put("return_msg", msg);
		return resJson;
		
	}
}
