package com.duanjy.webtools.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.duanjy.webtools.model.Cmdinfo;
import com.duanjy.webtools.model.Hostinfo;
import com.duanjy.webtools.service.CommandInfoService;
import com.duanjy.webtools.service.HostInfoService;
import com.jfinal.plugin.activerecord.Page;

public class SysManageController extends BaseController {
	private static final Logger logger = Logger.getLogger(HostManageController.class);
	Map resMap = new HashMap();
	// 分页信息
	public int page;
	public int limit;
	public String searchKey;
	CommandInfoService cmdInfoService = new CommandInfoService();
	
	public void findCmd() {
		// 从数据库获取主机信息列表
		
		
		// 定义list保存后台主机信息
		List<Cmdinfo> cmdinfos = new ArrayList<Cmdinfo>();

		// 前台获取页面和页数信息
		page = Integer.parseInt(getPara("page"));
		limit = Integer.parseInt(getPara("limit"));
		searchKey = getPara("searchkey");

		logger.info("1111111:"+page+","+limit+","+searchKey);
		Page<Cmdinfo> cmdPage = cmdInfoService.getCmdService(page,limit,searchKey);
				
		// 根据Page对象返回主机列表和主机总数
		cmdinfos = cmdPage.getList();
		int cmdCount = cmdPage.getTotalRow();

		
		resMap = getResult("0", "success", cmdCount, cmdinfos);

		if (cmdinfos != null) {
			renderJson(resMap);
		}
	}
	//用于添加命令内容
	public void addCmd(){
		JSONObject jsonString = new JSONObject();
		//通过model对象获取表单数据
		Cmdinfo cmdinfo = getModel(Cmdinfo.class,"",true);
		
		//补充后台数据库的时间
		Date systemTime = new Date();
		cmdinfo.setCreateTime(systemTime);
		cmdinfo.setModifyTime(systemTime);
			
		//调用service层操作数据库完成数据插入
		Boolean status = cmdInfoService.addCmdService(cmdinfo);
		
		jsonString = status ? getJsonResult("0000", "success") : getJsonResult("1111", "failed");
		
		renderJson(jsonString);
		
	}
	
	//用于在主机操作下拉列表展示
	public void cmdList(){

		List<Cmdinfo> cmdinfos = new ArrayList<Cmdinfo>();
		cmdinfos = cmdInfoService.getCmdList();
		
		logger.info("cmdinfos:"+cmdinfos);
		
		
		for(int i=0;i < cmdinfos.size();i++){
			//获取list中的命令内容和名称
			String cmd_name = cmdinfos.get(i).getCmdName();
			String cmd_content = cmdinfos.get(i).getCmdContent();
			resMap.put(cmd_name, cmd_content);
		}
		
		if (cmdinfos != null) {
			renderJson(resMap);
		}
	}
}
