package com.duanjy.webtools.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.duanjy.webtools.util.CallInterfaceUtil;
import com.duanjy.webtools.util.XmlToJsonUtil;

public class ToolManageController extends BaseController{
	private static final Logger logger = Logger.getLogger(ToolManageController.class);
	
	//处理接口调试中的post请求
	public void requestPost(){
		Map map = new HashMap();
		JSONObject jsonObject = new JSONObject();
		
		String url = getPara("url");
		String contentype = getPara("headers");
		String parameter = getPara("parameter");
		
		
		logger.info("报文入参："+parameter);
		CallInterfaceUtil callInterfaceUtil = new CallInterfaceUtil();
		map = callInterfaceUtil.httpClientPost(url, contentype, parameter);
		//logger.info("ToolManageController："+jsonObject);
		renderJson(map);
	}
	//处理接口调试中的get请求
	public void requestGet(){
		JSONObject jsonObject = new JSONObject();
		
		jsonObject = getJsonResult("0000", "没什么用，所以就没有开发,嘿嘿...");
		renderJson(jsonObject);
	}
	
	//处理报文转换请求
	public void xmlToJson(){
		JSONObject jsonObject = new JSONObject();
		
		String xml = getPara("input");
		
		XmlToJsonUtil xmUtil = new XmlToJsonUtil();
		String reString = xmUtil.xmlToJSON(xml, 1);
		jsonObject.put("message", reString);
		renderJson(jsonObject);
	}
	
}
