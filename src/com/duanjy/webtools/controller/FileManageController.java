package com.duanjy.webtools.controller;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.duanjy.webtools.business.FileUploadService;
import com.duanjy.webtools.util.DeleteFileUtil;
import com.duanjy.webtools.util.FileUtil;
import com.jfinal.upload.UploadFile;


public class FileManageController extends BaseController{
	private static final Logger logger = Logger.getLogger(FileManageController.class);
	long startTIme = 0;
	long endTime = 0;
	
	
	public void clientToServer(){
		JSONObject jsonObject = new JSONObject();
		
		UploadFile uploadFile = getFile();
		String uploadPath = uploadFile.getUploadPath();//获取保存文件的文件夹
		String fileName = uploadFile.getFileName();//获取保存文件的文件名
        String filePath = uploadPath+"\\"+fileName;//保存文件的路径
          
		logger.info("获取文件保存路径:"+filePath);
		jsonObject.put("filepath", filePath);
		renderJson(jsonObject);
		
		
	}
	public void serverToServ(){ 
		JSONObject jsonObject = new JSONObject();
		//获取文件上传信息
		String hostlist = getPara("hostinfos");
		String spath = getPara("spath");
		String dpath = getPara("dpath");
		logger.debug("文件源路径:" + spath);
		//调用文件上传接口
		startTIme = System.currentTimeMillis();
		FileUploadService fileService = new FileUploadService();
		String msgString = fileService.fileupload(hostlist, spath, dpath);
		endTime = System.currentTimeMillis();

		//文件上传成功后删除在本地服务器上的记录
		DeleteFileUtil deleteFileUtil = new DeleteFileUtil();
		boolean deleFileRes = deleteFileUtil.delete(spath);
		
		
		//文件上传操作耗时
		int totalTime = (int)(endTime - startTIme);
		//根据文件删除结果反馈给前端
		if( deleFileRes ){
			msgString = "文件缓存已清除:<br>"+msgString;
		}else{	
			msgString = "<font color=\"red\">文件缓存清除失败:</font><br>"+msgString;
		}
		jsonObject.put("return_code", "0000");
		jsonObject.put("time", totalTime);
		jsonObject.put("return_msg", msgString);
		logger.info("文件上传结果信息:"+msgString);
		renderJson(jsonObject);
	}
		
}
