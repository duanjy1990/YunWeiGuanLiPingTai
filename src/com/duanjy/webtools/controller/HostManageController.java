package com.duanjy.webtools.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.omg.CORBA.PUBLIC_MEMBER;

import ch.ethz.ssh2.Connection;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.duanjy.webtools.business.CommandExecService;
import com.duanjy.webtools.business.ReconnectHosts;
import com.duanjy.webtools.interceptor.GlobalInterceptor;
import com.duanjy.webtools.model.Hostinfo;
import com.duanjy.webtools.service.HostInfoService;
import com.duanjy.webtools.util.MultiDeleteHosts;
import com.duanjy.webtools.util.MultiInsertHosts;
import com.duanjy.webtools.util.SshdService;
import com.jcraft.jsch.Session;
import com.jfinal.core.Controller;
import com.jfinal.i18n.Res;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

public class HostManageController extends BaseController {
	private static final Logger logger = Logger.getLogger(HostManageController.class);
	//分页信息
	public int page;
	public int limit;
	public String searchKey;
	HostInfoService hosInfoService = new HostInfoService();
	
	
	/*
	 * 获取主机列表
	 * */
	public void findHost(){
		//从数据库获取主机信息列表
		HostInfoService hosInfoService = new HostInfoService();
		//定义list保存后台主机信息
		List<Hostinfo> hostinfo = new ArrayList<Hostinfo>();
		
		//前台获取页面和页数信息
		page = Integer.parseInt(getPara("page"));
		limit = Integer.parseInt(getPara("limit"));
		searchKey = getPara("searchkey");
		
		
		Page<Hostinfo> pageHost = hosInfoService.getHostService(page,limit,searchKey);
		//根据Page对象返回主机列表和主机总数
		hostinfo = pageHost.getList();
		int hostCount = pageHost.getTotalRow();
		
		Map resMap = new HashMap();
		resMap = getResult("0","success",hostCount,hostinfo);
		
		if( hostinfo != null){
			renderJson(resMap);
		}
	}
	/*
	 * 添加主机信息
	 * */
	
	public void addHost() {
		JSONObject jsonString = new JSONObject();
		int connStatus = 1;
		//通过model对象获取表单数据
		Hostinfo hsHostinfo = getModel(Hostinfo.class,"",true);
		
		//补充后台数据库的时间
		Date systemTime = new Date();
		hsHostinfo.setCreateTime(systemTime);
		hsHostinfo.setModifyTime(systemTime);
		
		//验证主机连接状态
		SshdService sshdService = new SshdService();
		Session session = sshdService.sshd(hsHostinfo.getIpAddress(), hsHostinfo.getLoginName(),
				hsHostinfo.getLoginPwd(), hsHostinfo.getLoginPort());
		if( session.isConnected()){
			connStatus = 0;
		}
		hsHostinfo.setConnStatus(connStatus);
		
		Boolean status = hosInfoService.addHostService(hsHostinfo);
		
		jsonString = status ? getJsonResult("0000", "success") : getJsonResult("1111", "failed");
		
		renderJson(jsonString);
	}
	/*
	 * 批量添加主机信息
	 * */
	public void multiAddHost(){
		Integer[] result = null;
		Map dataMap = new HashMap();
		
		//前台获取主机信息列表
		String hostList = getPara("hostslist");
		
		//调用工具类批量插入数据库
		MultiInsertHosts multiInsertHosts = new MultiInsertHosts();
		
		try {
			result = multiInsertHosts.BatchInsert(hostList);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		dataMap.put("total", result[0]);
		dataMap.put("success", result[1]);
		dataMap.put("failed", result[2]);
		logger.info("批量执行返回结果:"+result[0]+","+result[1]+","+result[2]);
		JSONObject resJson = getJsonData("0000","success",result[0],dataMap);
	
		renderJson(resJson);
	}
	/*
	 * 批量删除主机信息
	 * */
	public void multiDelete(){
		Integer[] result = null;
		Map dataMap = new HashMap();
		
		String hostidString = getPara("hostinfos");
		MultiDeleteHosts multiDeleteHosts = new MultiDeleteHosts();
		
		try {
			result = multiDeleteHosts.batchDel(hostidString);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		dataMap.put("total", result[0]);
		dataMap.put("success", result[1]);
		dataMap.put("failed", result[2]);
		logger.info("批量删除返回结果:"+result[0]+","+result[1]+","+result[2]);
		JSONObject resJson = getJsonData("0000","success",result[0],dataMap);
	
		renderJson(resJson);
	}
	/*
	 * 删除主机信息
	 * */
	public void delHost(){
		JSONObject jsonString = new JSONObject();
		
		int host_id =  Integer.parseInt(getPara("id"));
		Boolean status = hosInfoService.delHostService(host_id);
		
		jsonString = status ? getJsonResult("0000", "success") : getJsonResult("1111", "failed");
		renderJson(jsonString);
	}
	/*
	 * 发送命令执行
	 * */
	
	public void cmdExec(){
		JSONObject jsonString = new JSONObject();
		String result = "";
		String hostIds = getPara("hostinfos");
		String cmdString = getPara("cmd");
		
		CommandExecService cmdService = new CommandExecService();
		try {
			result = cmdService.cmdExec(hostIds, cmdString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("命令操作结果:"+result);
		renderText(result);
	}
	
	/*
	 *主机重连 
	 */
	
	public void reConn(){
		JSONObject jsonString = new JSONObject();
		int hostId = Integer.parseInt(getPara("id"));
		
		ReconnectHosts reHosts = new ReconnectHosts();
		Boolean status = reHosts.getReconn(hostId);
		
		if( status ){
			jsonString =  getJsonResult("0000", "主机状态更新成功");
		}else{
			jsonString =  getJsonResult("1111", "主机状态更新失败");
		}
		renderJson(jsonString);
	}
}
