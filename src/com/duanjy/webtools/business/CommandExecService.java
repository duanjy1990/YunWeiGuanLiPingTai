package com.duanjy.webtools.business;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import ch.ethz.ssh2.log.Logger;

import com.duanjy.webtools.controller.HostManageController;
import com.duanjy.webtools.model.Hostinfo;
import com.duanjy.webtools.service.HostInfoService;
import com.duanjy.webtools.util.RemoteCmdExecUtil;


public class CommandExecService {
	private static final Logger logger = Logger.getLogger(CommandExecService.class);
	String resultMsg = "";
	
	public String cmdExec(String hostid, final String cmd) throws Exception {
		int count = 0;
		// 用逗号分隔符将传入的主机id字符串进行分割
		String[] ids = hostid.split(",");
		count= ids.length;
		final CountDownLatch countDownLatch = new CountDownLatch(count);
		final HostInfoService hoService = new HostInfoService();
	  
		for (final String id : ids) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					// 从数组中取出主机id值
					int new_id = Integer.parseInt(id.replaceAll("\"", ""));

					// 调用dao层方法，通过id值获取对应的主机信息
					Hostinfo hostInfo = new Hostinfo();
					hostInfo = hoService.getColumns(new_id);
									
					try {
						// 创建远程命令执行工具类
						RemoteCmdExecUtil reCmdExecUtil = new RemoteCmdExecUtil();
						String result = reCmdExecUtil.cmdExec(
								hostInfo.getIpAddress(),
								hostInfo.getLoginName(),
								hostInfo.getLoginPwd(),
								hostInfo.getLoginPort(), cmd);
						resultMsg += "######主机 [" + hostInfo.getIpAddress() + "] 执行结果######\n" + result;
					} catch (Exception e) {
						e.printStackTrace();
					}finally{
						// 线程工作完成，计数器值减一
						countDownLatch.countDown();
					}

					
				}
			}).start();
			
		}
		countDownLatch.await();
		System.out.println("主机命令执行返回结果:"+resultMsg);
		return resultMsg;
	}
}
