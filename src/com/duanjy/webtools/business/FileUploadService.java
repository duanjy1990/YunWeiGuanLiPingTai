package com.duanjy.webtools.business;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import com.duanjy.webtools.model.Hostinfo;
import com.duanjy.webtools.service.HostInfoService;
import com.duanjy.webtools.util.FileUploadUtil;
import com.sun.org.apache.xerces.internal.util.Status;

public class FileUploadService {
	public String resultMsg = "";
	public Map map = new HashMap();
	public int status;
	public String fileupload(String hostid, final String spath, final String dpath)  {

		Integer count = 0;
		// 用逗号分隔符将传入的主机id字符串进行分割
		String[] ids = hostid.split(",");
		count = ids.length;
		final CountDownLatch countDownLatch = new CountDownLatch(count);
		
		for (final String id : ids) {
			new Thread( new Runnable() {
				
				@Override
				public void run() {
					int new_id = Integer.parseInt(id.replaceAll("\"", ""));

					// 调用dao层方法，通过id值获取对应的主机信息
					HostInfoService hostInfoService = new HostInfoService();
					Hostinfo hostinfo = hostInfoService.getColumns(new_id);
				
					FileUploadUtil fileup = new FileUploadUtil();
					try {
						status = fileup.sftpFile(hostinfo.getIpAddress(),
								hostinfo.getLoginName(), hostinfo.getLoginPwd(),
								hostinfo.getLoginPort(), spath, dpath);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if( status == 0){
						resultMsg += "主机 ["+ hostinfo.getIpAddress() + "] 上传成功<br>";
					}else if(status == 2){
						resultMsg += "主机 ["+ hostinfo.getIpAddress() + "] 路径: "+dpath+"不存在<br>";
					}else if(status == 3){
						resultMsg += "主机 ["+ hostinfo.getIpAddress() + "] 连接失败<br>";
					}else{
						resultMsg += "主机 ["+ hostinfo.getIpAddress() + "] 上传失败<br>";
					}
					// 线程工作完成，计数器值减一
					countDownLatch.countDown();
				}
			}).start();
			
		}
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(resultMsg);
		return resultMsg;
	}
}
