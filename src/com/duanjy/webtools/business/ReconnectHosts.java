package com.duanjy.webtools.business;

import com.duanjy.webtools.model.Hostinfo;
import com.duanjy.webtools.service.HostInfoService;
import com.duanjy.webtools.util.SshdService;
import com.jcraft.jsch.Session;

public class ReconnectHosts {
	public Boolean getReconn(int hostid){
		
		int connStatus = 1;
		HostInfoService hosService = new HostInfoService();
		
		
		//通过前端主机id获取主机信息
		Hostinfo hostinfo  = new Hostinfo();
		hostinfo = hosService.getColumns(hostid);
		
		//调用sshd服务测试主机的连通性
		SshdService ssService = new SshdService();
		Session session = ssService.sshd(hostinfo.getIpAddress(), hostinfo.getLoginName(), 
				hostinfo.getLoginPwd(), hostinfo.getLoginPort());
		
		if( session.isConnected()){
			connStatus = 0;
		}
		
		Boolean resStatus = hosService.updateConnStaus(hostid,connStatus);
		return resStatus;
	}
}
