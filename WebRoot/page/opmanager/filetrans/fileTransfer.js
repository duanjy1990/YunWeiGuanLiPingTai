/**
 * Created by Duanjy on 2018/2/1.
 */
layui.use(['table','layer','element','upload','form'], function(){
    var table = layui.table,
        element = layui.element,
        $ = layui.jquery,
    	upload = layui.upload,
    	form = layui.form;
    var uploadindex = null;	
    
    var funcList = {
    //渲染主机列表
    	"loadHostList":function(){
    		var loadindex = layer.msg('正在加载中...', {
    			icon : 16,
    			time : 1000,
    			shade : 0.4
    		});
    	    table.render({
    	        elem: '#file-host-table'
    	        ,url: '../../../host/findHost' //数据接口
    	        ,page: true //开启分页
    	        ,cellMinWidth : 130
    	        ,limit:14
		        ,limits:[14,20,30,40,50,60,100]
    	        ,cols: [[ //表头
    	            {checkbox : true,fixed : true}
    	            ,{field: 'id', title: 'ID', width:70, sort: true, align:'center'}
    	            ,{field: 'host_group', title: '主机分组', width:110,align:'center'}
    	            ,{field: 'host_type', title: '主机类型', width:130, align:'center'}
    	            ,{field: 'ip_address', title: '主机地址', width:130, align:'center'}
    	            ,{field: 'login_name', title: '用户名', width: 110,align : 'center'}
    	            ,{field: 'login_pwd', title: '密码', width: 110, align : 'center',templet: '#hide_passwd'}
    	            ,{field: 'login_port', title: '端口', width: 90, align : 'center'}
    	            ,{field: 'conn_status', title: '连接状态', width: 140,align : 'center',templet: '#connStatus'}
    	            ,{field: 'create_time', title: '创建时间', width: 150,align : 'center'}
    	            ,{field: 'modify_time', title: '修改时间', width: 200,align : 'center'}
    	            ,{field: 'mark', title: '备注', width: 397,align : 'center'}
    	        ]],
    	        id : 'fileupload',
    	        height:650,
    	        done:function(){
    	            layer.close(loadindex);
    	        }
    	    });
    	},
    	//主机管理页面搜索框重载表格
		"search-btn":function(){
			var loadindex = layer.msg('玩命加载中...', {
				icon : 16,
				time : 99999,
				shade : 0.4
			});
			
			var keyValue = $('#key-value');
			table.reload('fileupload',{
				page:true,
				limit:10000,
				limits:[10,10000],
				where:{searchkey: keyValue.val()},
				done:function(){
					layer.close(loadindex);
				}
			})
		},
    	//打开文件上传弹出层
		"fileUpload":function(){
			var checkStatus = table.checkStatus('fileupload'); //test即为基础参数id对应的值
			var length =  checkStatus.data.length;
			
			//定义一个数组用于存储所选主机的id值
			var hostids = new Array();
			for(var i=0;i < length;i++){
				//获取选中行的id
				hostids.push(JSON.stringify(checkStatus.data[i].id));
			}
			
			if( length == 0){
				layer.msg("未选择任何主机",{icon:2,time:2000});
			}else{
			uploadindex = layer.open({
	                type : 1,
	                title : '文件上传窗口',
	                skin:'layui-layer-lan',
	                area : [ '400px', '335px' ],
	                content : '<div id="fielupload-form" class="layui-form" lay-filter="test1"></div>',
	                success : function(index, layero) {
	                    $('#fielupload-form').load('file-upload.html',function() {
	                            form.render(null,'test1');
	                            //监听开始上传按钮动作
	                            $("#upload-btn").click(function(){
	                            	var func = funcList[$(this).attr("id")];
	                            	func();
	          
	                            })
	                        })

	                }
	            });
			}
		},
		//文件上传按钮	
		"upload-btn":function(){
			var sourPath = $("#source-file").val();
			var destPath = $("#destpath").val();
			var checkStatus = table.checkStatus('fileupload'); //test即为基础参数id对应的值
			var length =  checkStatus.data.length;

			//定义一个数组用于存储所选主机的id值
			var hostids = new Array();
			for(var i=0;i < length;i++){
				//获取选中行的id
				hostids.push(JSON.stringify(checkStatus.data[i].id));
			}
			if( sourPath == ""){
				layer.msg("未选择文件",{icon:2,time:2000});
			}else if(destPath == ""){
				layer.msg("未指定目标路径",{icon:2,time:2000});
			}else{
				layer.close(uploadindex);
				var loadindex = layer.msg('文件上传中...', {
					icon : 16,
					time : 9999999,
					shade : 0.4
				});
				$.ajaxFileUpload  
		        ({  
		            url:'../../../file/clientToServer',
		            secureuri:false,  
		            fileElementId:'source-file',  
		            dataType: 'json',
		            data:{
		            	filename:sourPath
		            },
		            success: function (data,status)  
		            { 
		            	$.ajax({
							type : "POST",
							url : "../../../file/serverToServ",
							data : {hostinfos:hostids.toString(), spath:data.filepath,dpath:destPath},
							success : function(msg) {
								layer.close(loadindex);
								layer.open({
									  title: '上传结果 [耗时:'+msg.time+' ms]'
									  ,area:['350px','400px']
									  ,content: msg.return_msg
									  ,skin:'layui-layer-molv'
									  ,anim:5
									  ,btn:""
									}); 
	
								}
						});
		            },
			        error: function(data,status){ 
			        	layer.msg("上传失败",{icon:2,time:2000});
	                }
		        });
					
			}
			
		},
		//文件上传成功后的回显
		"upload-result":function(msg){
			layer.open({
				  title: '上传结果'
				  ,area:['350px','400px']
				  ,content: msg
				  ,skin:'layui-layer-molv'
				  ,anim:5
				  ,btn:""
				}); 
		},
		"tableReload":function(){
			table.reload('fileupload',{
					page:{curr:1},
					done:function(){
						//layer.close(loadindex);
					}
				})
		},
    	
    }
    
    var func = funcList["loadHostList"];
    func();
    
    //监控所有按钮操作
    $(".active-btn").click(function(){
        var id = $(this).attr("id");
       if( id != null){
    	   var func = funcList[id];
           func();
       }else{
    	   layer.msg("功能尚未定义");
       }
        
    })
	

});