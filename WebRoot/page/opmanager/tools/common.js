/**
 * @其他工具下拉框中所有页面的js
 */
layui.use(['table','layer','form','element'], function(){
	 var table = layui.table;
	 var layer = layui.layer;
	 var $ = layui.jquery;
	 var form = layui.form;
	 var element = layui.element;
	 
	
	 var funcList = {
		//接口测试页面send按钮
	    "sendRequest":function(){
	    	var url = $("#requestUrl").val();
	    	var method = $("#method-select option:selected").val();
	    	var header = $("#header-select option:selected").val();
	    	var format = $('input:radio[name="format"]:checked').val();
	    	var inputData = $("#inParameter").val();
	    	
	    	if( method == "POST"){
	    		if( url == ""){
		    		layer.msg("URL不能为空!!!",{icon:2,time:1000});
		    	}else if( inputData == "" ){
		    		layer.msg("入参不能为空!!!",{icon:2,time:1000});
		    	}else{
		    		var loadindex = layer.msg('请求执行中...', {
						icon : 16,
						time : 99999,
						shade : 0.4
					});
		    		
		    		$.ajax({
						type : "POST",
						url : "../../../tool/requestPost",
						data : {"url":url,"method":method,"headers":header,"format":format,"parameter":inputData},
						success : function(jsonString) {
							layer.close(loadindex);
							$("#interface_res").val(jsonString.message);
							if( jsonString.httpcode == 200 ){
								$("#callTime").html("Status:<font color=\"red\">"+jsonString.httpcode+
										"</font> Time:<font color=\"red\">"+jsonString.time+"ms</font>");
							}else{
								$("#callTime").html("");
							}
							

						}
					});
		    		
		    	}
	    	}else if(method == "GET"){
	    		if( url == ""){
		    		layer.msg("URL不能为空!!!",{icon:2,time:1000});
		    	}else{
		    		var loadindex = layer.msg('请求执行中...', {
						icon : 16,
						time : 99999,
						shade : 0.4
					});
		    		
		    		$.ajax({
						type : "POST",
						url : "../../../tool/requestGet",
						data : {"url":url,"method":method,"headers":header,"format":format,"parameter":inputData},
						success : function(msg) {
							layer.close(loadindex);
							$("#interface_res").val(msg.return_msg);
						}
					});
		    	}
	    	}
	    	
	    },
	    "xml2json":function(){
	    	var xmlString = $("#xml-message").val();
	    	//var json_obj = $.xml2json(xmlString,true);
	    	layer.msg(json_obj);
	    }
			 
	 }
	
	 //批量按钮操作
	 $(".otherpage-btn").click(function(){
		 var  id = $(this).attr("id");
		 var func = funcList[id];
		 func();
	 })
	 
})
