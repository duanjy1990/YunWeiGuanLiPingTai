layui.use(['table','layer','form','element'], function(){
    var table = layui.table;
    var layer = layui.layer;
    var $ = layui.jquery;
    var form = layui.form;
    var element = layui.element;
    var userRole = "";
    
    
    var funcList = {
		//主机配置页面表格渲染
		"loadCmdList":function(){
//			 var loadindex = layer.msg('正在加载中...', {
//					icon : 16,
//					time : 999999,
//					shade : 0.4
//				});
			    table.render({
			        elem: '#cmdinfo-table'
			        //,url: '../../host/user' //数据接口
			        ,url:'../../setup/findCmd'
			        ,page: true //开启分页
					,cellMinWidth : 130
			        ,limit:14
			        ,limits:[14,20,30,40,50,60,100]
			        ,cols: [[ //表头
			            {checkbox : true,fixed : true}
			            ,{field: 'id', title: 'ID', width:70, sort: true, align:'center'}
			            ,{field: 'cmd_content', title: '命令内容',align:'center'}
			            ,{field: 'cmd_name', title: '命令别名',  align:'center'}
			            ,{field: 'create_time', title: '创建时间',align : 'center'}
			            ,{field: 'modify_time', title: '修改时间',align : 'center'}
			            ,{field: 'cmd_mark', title: '备注', width: 205,align : 'center'}
			            ,{field: 'operation', title: '相关操作', width: 279,align : 'center',toolbar : '#barDemo'}
			        ]],
			        id : 'allcmdlist',
			        height:"full-130",
			        done:function(){
			           // layer.close(loadindex);
			        }
			    });	
	
		},
		//添加命令的表单
		"cmdAdd":function(){
			currentIndex = layer.open({
                type : 1,
                title : '添加命令',
                area : [ '400px', '300px' ],
                skin:'layui-layer-lan',
                content : '<div id="add-cmd-form"></div>',
                success : function(index, layero) {
                    $('#add-cmd-form').load('add-cmd.html', function() {
                    	form.render('select');
                    	var cmdFormSubmit = funcList["cmdSubmit"];
                    	cmdFormSubmit();
                    })

                }
            });
		},
		//命令录入表达提交
		"cmdSubmit":function(){
			form.on('submit(add-confirm)',function(data){
				//敏感命令信息过滤  ，后期增加
        		var loadindex = layer.msg('正在保存中...', {
        			icon : 16,
        			time : 99999,
        			shade : 0.4
        		});
        		//提交表单数据到后台
        		$.ajax({
        			type : "POST",
        			url : "../../setup/addCmd",
        			data : data.field,
        			success : function(msg) {
        				layer.close(loadindex);
        				//注意：由于后台使用renderjson,前端获取不需要JSON.stringfy
        				if (msg.return_code == "0000") {
        					layer.close(currentIndex);
        					//添加成功后从新渲染表格
        					var tablereload = funcList["loadCmdList"];
        					tablereload();
        					layer.msg("保存成功", {
        						icon : 1,
        						time : 1800
        					});	
        				}
        				else{
        					layer.msg("保存失败", {
        						icon : 2,
        						time : 1800
        					});
        				}
        			}
        		});
        		//主机添加页面提交表单数据			
        		return false;
        	})
		}
		
    }
//-------------方法列表结束-------------------------------------------
 	//加载主机配置页面的主机列表
    
    var func = funcList["loadCmdList"];
    func();
    
    
    //监听按钮动作
    $(".active-btn").click(function(){
        var id = $(this).attr("id");
        var flag = $(this).attr("flag");
        if( id != null ){
        	//刷新页面不受权限控制
        	var func = funcList[id];
        	func();
      
        }else{
     	   layer.msg("功能尚未定义");
        }
    })
     
  
});