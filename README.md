# 运维管理平台

#### 项目介绍
一个基于layui+jQuery+ajax+Jfinal开发的运维管理平台，主要完成用于完成批量主机命令执行，文件上传/下载，远程接口调试和报文格式转换,辅助日常测试及运维

#### 软件架构
软件架构说明


#### 安装教程

1.安装jdk-1.7.80

2.安装tomcat8.0

3.安装mysql 5.x

--数据库用户名/密码:root/123456

4.建表语句

--用户信息表 CREATE TABLE userinfo ( id int(20) NOT NULL AUTO_INCREMENT, user_name varchar(50) NOT NULL, user_pwd varchar(50) NOT NULL, role_group varchar(50) NOT NULL, create_time date DEFAULT NULL, modify_time date DEFAULT NULL, PRIMARY KEY (id) ) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8; INSERT INTO userinfo(user_name,user_pwd,role_group,create_time,modify_time) VALUES('admin','123456','administrator',SYSDATE(),SYSDATE());

--主机信息表 CREATE TABLE hostinfo ( id int(20) NOT NULL AUTO_INCREMENT, host_group varchar(50) NOT NULL, host_type varchar(50) NOT NULL, ip_address varchar(50) NOT NULL, login_name varchar(50) NOT NULL, login_pwd varchar(50) NOT NULL, login_port int(50) NOT NULL, conn_status int(5) NOT NULL, create_time datetime DEFAULT NULL, modify_time datetime DEFAULT NULL, mark varchar(50) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE=InnoDB AUTO_INCREMENT=433 DEFAULT CHARSET=utf8;

--命令信息表 CREATE TABLE cmdinfo ( id int(20) NOT NULL AUTO_INCREMENT, cmd_content varchar(50) NOT NULL, cmd_name varchar(50) NOT NULL, create_time datetime DEFAULT NULL, modify_time datetime DEFAULT NULL, cmd_mark varchar(50) DEFAULT NULL, PRIMARY KEY (id) ) ENGINE=InnoDB AUTO_INCREMENT=333 DEFAULT CHARSET=utf8;

#### 平台演示
http://omp.duanjy.cn/webtools4/index.html

#### 使用说明

平台仅限于技术交流和个人学习使用，请勿用于商业用途，感谢配合。
